import string
import numpy as np


def build_reference():
    """Build the default reference
    """
    # get lowercase letters and decimal digits
    all = list(string.printable)
    all.append("à")
    all.append("á")
    all.append("é")
    all.append("è")
    all.append("í")
    all.append("ó")
    all.append("ò")
    all.append("ú")
    all.insert(0, all.pop(len(all) - 1))
    return all


def cipher_character(char, key_index, reference = build_reference()):
    """Cipher a single character
    """
    char_index = reference.index(char)
    ciphered_char_index = (char_index - key_index) % len(reference) 
    return reference[ciphered_char_index]


def decipher_character(ciphered_char, key_index, reference = build_reference()):
    """Decipher a single character
    """
    ciphered_char_index = reference.index(ciphered_char)
    deciphered_char_index = (key_index + ciphered_char_index) % len(reference)
    return reference[deciphered_char_index]


def cipher_string(clear_string, keys, reference = build_reference()):
    """Cipher a string of characters.
    Note that spaces are taken as characters
    """

    clear_string_list = list(clear_string)

    # Convert a string of keys to a list of keys to simplify picking one at a
    # time
    keys_list = list(keys)
    ciphered_list = []

    for ind, char in enumerate(clear_string_list):
        key = keys_list[ind % len(keys_list)]
        key_index = reference.index(key)
        ciphered_list.append(cipher_character(char, key_index, reference))

    return ''.join(map(str, ciphered_list))


def decipher_string(ciphered_string, keys, reference = build_reference()):
    """Decipher a string of characters
    Note that spaces are encoded
    """
    ciphered_string_list = list(ciphered_string)
    keys_list = list(keys)
    clear_list = []

    for ind, char in enumerate(ciphered_string_list):
        key = keys_list[ind % len(keys_list)]
        key_index = reference.index(key)
        clear_list.append(decipher_character(char, key_index, reference))

    return ''.join(map(str, clear_list))

