FROM python:3.6-slim

ENV LANG en_US.UTF-8
ENV LC_ALL C.UTF-8

RUN apt-get update -qq \
 && apt-get install -qqy \
       gcc \
       graphviz \
 && apt-get autoclean \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
 && pip install --upgrade pip

COPY requirements.txt /usr/local/hidder/requirements.txt

WORKDIR /usr/local/hidder

RUN pip install -r requirements.txt

COPY hidder.py /usr/local/hidder/hidder.py

#CMD ["./ambiciosa/ambiciosa.py"]
