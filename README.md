# Hidder

## Summary
This repo includes two functions to hide or unhide a message using a simple
encryption algorithm. The two functions are:
  - `cipher_string(message, key, reference)`
  - `decipher_string(ciphered_message, key, reference)`

Both have three parameters:
  - The `message`: a string to cipher or decipher;
  - The `key`: a string;
  - The `reference` which is optional. If no parameter is included it takes the outcome of the function `build_reference()`.

## The algorithm
The algorithm basically takes all characters from `string.printable` plus the accentuated vowels.
The reference maps each character to another one depending on the key.

If the key is shorter than the sentence to encode, it starts again.

## How to use it
To use the dockerised version, just run:
```
make build
make repl
```

and when you're on ipython3

```python
from hidder import cipher_string, decipher_string

cipher_string("message you want to cipher", "key")
decipher_string(cipher_string("message you want to cipher", "key"))
```


