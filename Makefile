VERSION := dev
id := hidder

image := $(id):$(VERSION)

DOCKER_TASK = docker run -v $(pwd)/coses:/usr/local/hidder/coses --rm -it

build:
	docker build --tag $(image) .
.PHONY: build

repl:
	@$(DOCKER_TASK) $(image) ipython 
.PHONY: repl

